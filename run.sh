#!/usr/bin/env bash

# Dependencies
cd backend
git pull
bundle install --without test development

# Cron
pkill -f cron
exec -a cron ruby worker.rb &

# Server
bundle exec rake db:migrate
INTERNAL_API_KEY=welcome RACK_ENV=production bundle exec rackup -p 80
