require 'rubygems'
require 'sinatra'
require 'sinatra/cross_origin'
require 'active_record'
require 'grape'
require 'mail'
require 'rufus-scheduler'

require './validators'
require './models/init'
require './jobs/init'

Mail.defaults do
  delivery_method :smtp,
                  port: 587,
                  address: 'smtp.mailgun.org',
                  user_name: 'postmaster@mg.startupslike.me',
                  password: 'f9303f79d71d602f4e99a6f2e9afd286'
end

ActiveRecord::Base.establish_connection(adapter: 'sqlite3', database: 'sqlite3.db')

configure do
  enable :cross_origin
end
