# rubocop:disable Metrics/MethodLength

# context for email templates
class TemplateScope
  def initialize(hash)
    hash.each do |key, value|
      singleton_class.send(:define_method, key) { value }
    end
  end

  def template_binding
    binding
  end
end

def send_notifications_to(candidates)
  candidates.find_each do |candidate|
    puts " - Sending email to #{candidate.email}..."
    startups = get_startups_for_candidate(candidate)
    if candidate.email
      email_record = Email.create(candidate: candidate)
      email_record.startups = startups
      scope = TemplateScope.new(startups: startups, candidate: candidate, email_id: email_record.id)
      mail = Mail.new do
        from 'Startups Like Me <feed@startupslike.me>'
        to candidate.email
        subject "#{Time.now.strftime('%m/%d/%Y')} - Cool Startups For You"
        text_part do
          content_type 'text/plain'
          body ERB.new(
            File.read('views/startup_feed_mail_text.erb')
          ).result(scope.template_binding)
        end
        html_part do
          content_type 'text/html; charset=UTF-8'
          body ERB.new(File.read('views/startup_feed_mail.erb')).result(scope.template_binding)
        end
      end
      mail.deliver!
    else
      # TODO send notifications here
    end
  end
end

def get_startups_for_candidate(_candidate)
  # We just do random selecting for now
  (1..3).to_a.map { Startup.where(deleted_at: nil).order('RANDOM()').first }
end
