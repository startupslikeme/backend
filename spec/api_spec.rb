describe 'candidates' do
  it 'should allow creation via email' do
    create(:startup)
    post '/v1/candidates', email: 'test1@example.com'
    expect_json(error: nil, email: 'test1@example.com')
    expect_status(201)
  end
  it 'candidates with invalid emails should not be saved' do
    create(:startup)
    post '/v1/candidates', email: 'not an email'
    expect_json(error: { email: ['is not an email'] })
    expect_status(400)
  end
  it 'sholud allow for the updating of email frequency' do
    candidate = create(:candidate, frequency: 'weekly')
    patch '/v1/candidates/' + candidate.id.to_s,
          access_token: candidate.access_token,
          frequency: 'daily'
    expect_json(error: nil, frequency: 'daily')
    expect_status(200)
  end
end

describe 'startups' do
  it 'List startups should be filterable by an email' do
    name = 'Rocket Book'
    candidate = create(:candidate)
    startup = create(:startup, name: name)
    email = create(:email, candidate: candidate, startups: [startup])
    get '/v1/startups?email_id=' + email.id.to_s + '&access_token=' + candidate.access_token.to_s
    expect_status(200)
  end
  it 'Should be able to create a startup with the appropriate auth' do
    ENV['INTERNAL_API_KEY'] = 'test'
    post '/v1/startups/',
         api_key: ENV['INTERNAL_API_KEY'],
         name: 'Zume pizza',
         website_url: 'https://www.zumepizza.com/',
         locations: ['Mountain View'],
         industries: ['Food Tech']
    expect_status(201)
  end
  it 'don\'t allow creation of startup with invalid key' do
    ENV['INTERNAL_API_KEY'] = 'test'
    post '/v1/startups/',
         api_key: 'this is an invalid key',
         name: 'Zume pizza',
         website_url: 'https://www.zumepizza.com/',
         locations: ['Mountain View'],
         industries: ['Food Tech']
    expect_json(error: 'API Key is wrong')
    expect_status(403)
  end
  it 'don\'t allow startups with bad urls' do
    ENV['INTERNAL_API_KEY'] = 'test'
    post '/v1/startups/',
         api_key: ENV['INTERNAL_API_KEY'],
         name: 'Zume pizza',
         website_url: 'not the zume pizza url',
         locations: ['Mountain View'],
         industries: ['Food Tech']
    expect_status(400)
  end
end

describe 'ratings' do
  it 'Ratings should be listable by email' do
    name = 'Rocket Book'
    candidate = create(:candidate, email: 'knf@example.com')
    startup = create(:startup, name: name)
    email = create(:email, candidate: candidate, startups: [startup])
    get '/v1/ratings?email_id=' + email.id.to_s + '&access_token=' + candidate.access_token.to_s
    expect_status(200)
  end
  it 'a candidate should be able to rate a startup' do
    candidate = create(:candidate)
    startup = create(:startup)
    post '/v1/ratings',
         access_token: candidate.access_token,
         startup_id: startup.id,
         intrest_level: 1
    expect_status(201)
  end
  it 'a candidate should be able to update thier rating of a startup' do
    candidate = create(:candidate, email: 'test2@example.com')
    startup = create(:startup)
    rating = create(:rating, candidate: candidate)
    patch '/v1/ratings/' + rating.id.to_s,
          access_token: candidate.access_token,
          startup_id: startup.id,
          intrest_level: 1
    expect_status(200)
  end
end
