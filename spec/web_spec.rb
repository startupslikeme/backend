describe 'test app urls' do
  it 'home page should redirect to website' do
    get '/'
    expect_status(302)
  end
  it 'ping should pong' do
    get '/ping'
    expect_status(200)
    expect(body).to eq('pong')
  end
  it 'should 200' do
    options '*'
    expect_status(200)
  end
end
