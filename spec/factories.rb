FactoryGirl.define do
  factory :startup do
    name 'Startups Like Me'
  end
  factory :candidate do
    email 'test@example.com'
    frequency 'daily'
  end
  factory :email do
  end
  factory :rating do
    intrest_level 0
  end
end
