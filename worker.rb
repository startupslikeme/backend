require './app'

scheduler = Rufus::Scheduler.new

# 9 AM every day
scheduler.cron '00 04 * * *' do
  send_notifications_to(Candidate.where(frequency: 'daily'))
end

# 9 AM every Sunday
scheduler.cron '00 04 * * 0' do
  send_notifications_to(Candidate.where(frequency: 'weekly'))
end

# 9 AM every month on the first day
scheduler.cron '00 04 1 * *' do
  send_notifications_to(Candidate.where(frequency: 'monthly'))
end

scheduler.join
