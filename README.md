# backend
[![build status](https://gitlab.com/startupslikeme/backend/badges/master/build.svg)](https://gitlab.com/startupslikeme/backend/commits/master)
[![coverage report](https://gitlab.com/startupslikeme/backend/badges/master/coverage.svg)](https://gitlab.com/startupslikeme/backend/commits/master)

backend is the core API service of startupslike.me. It provides RESTful APIs and misc. endpoints for resources.


## Setup

```bash
$ bundle install
$ rake db:migrate
```

## Run local server

```bash
$ RACK_ENV=development INTERNAL_API_KEY=test bundle exec rackup
```

## Server

connect to server
```bash
$ ssh root@api.startupslike.me
```

## Testing
```bash
bundle exec rspec
```

if no run.sh
```bash
$ cp backend/run.sh run.sh
$ chmod 766 run.sh
```

## Test Email

```bash
tux -c app.rb
send_notifications_to(Candidate.where(email:"test@example.com"))
```


## Testing API example commands

### POST candidate

```bash
$ curl -H "Content-Type: application/json" -X POST -d '{"email":"tim@example.com"}' http://localhost:9292/api/v1/candidates
```

### PUT candidate

```bash
$ curl -H "Content-Type: application/json" -X PUT -d '{"frequency":"daily"}' http://localhost:9292/api/v1/candidates/1
```

### GET candidate

http://localhost:9292/api/v1/candidates/1
