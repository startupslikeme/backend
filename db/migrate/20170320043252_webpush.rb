class Webpush < ActiveRecord::Migration
  def change
    add_column :candidates, :notification_method, :string
    add_column :candidates, :onesignal_id, :string
  end
end
