class LocationIndustryFk < ActiveRecord::Migration
  def change
    create_table :startup_industries do |t|
      t.integer :industry_id
      t.integer :startup_id
    end
    create_table :startup_locations do |t|
      t.integer :location_id
      t.integer :startup_id
    end
  end
end
