class InitMigration < ActiveRecord::Migration

  def change
    create_table :authentications do |t|
      t.string :password
      t.timestamps null: false
    end

    create_table :candidates do |t|
      t.belongs_to :authentication
      t.string :email
      t.string :frequency
      t.timestamps null: false
    end

    create_table :emails do |t|
      t.integer :candidate_id
      t.boolean :opened
      t.timestamps null: false
    end

    create_table :email_startups do |t|
      t.integer :email_id
      t.integer :startup_id
    end

    create_table :view_logs do |t|
      t.integer :email_id
      t.integer :created_at
    end

    create_table :startups do |t|
      t.string :name
      t.text   :description
      t.string :location_region
      t.string :location_city
      t.string :location_country_code
      t.string :stock_symbol
      t.string :industry
      t.string :website_url
      t.string :image_url
      t.string :funding_round
      t.integer :number_of_employees
      t.integer :total_funding_raised
      t.timestamps null: false
    end

    create_table :ratings do |t|
      t.integer :canidate_id
      t.integer :startup_id
      t.float :intrest_level
      t.timestamps null: false
    end
  end
end
