class ModifyStartups < ActiveRecord::Migration
  def change
    add_column :startups, :crunchbase_url, :string
    add_column :startups, :angellist_url, :string
    add_column :startups, :locations, :string
    remove_column :startups, :location_region, :string
    remove_column :startups, :location_city, :string
    remove_column :startups, :location_country_code, :string
    remove_column :startups, :stock_symbol, :string
  end
end
