class RemoveNullOpened < ActiveRecord::Migration
  def change
    change_column_null :emails, :opened, false
    change_column_default :emails, :opened, from: nil, to: false

  end
end
