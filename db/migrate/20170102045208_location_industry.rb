class LocationIndustry < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.string :name
      t.timestamps null: false
    end
    create_table :industries do |t|
      t.string :name
      t.timestamps null: false
    end
    remove_column :startups, :locations, :string
    remove_column :startups, :industry, :string
  end
end
