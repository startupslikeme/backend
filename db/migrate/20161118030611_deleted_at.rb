class DeletedAt < ActiveRecord::Migration
  def change
    add_column :authentications, :deleted_at, :datetime
    add_column :candidates, :deleted_at, :datetime
    add_column :emails, :deleted_at, :datetime
    add_column :ratings, :deleted_at, :datetime
    add_column :startups, :deleted_at, :datetime

  end
end
