# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170320043252) do

  create_table "authentications", force: :cascade do |t|
    t.string   "password"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
  end

  create_table "candidates", force: :cascade do |t|
    t.integer  "authentication_id"
    t.string   "email"
    t.string   "frequency"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.datetime "deleted_at"
    t.string   "access_token"
    t.string   "notification_method"
    t.string   "onesignal_id"
  end

  create_table "email_startups", force: :cascade do |t|
    t.integer "email_id"
    t.integer "startup_id"
  end

  create_table "emails", force: :cascade do |t|
    t.integer  "candidate_id"
    t.boolean  "opened",       default: false, null: false
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.datetime "deleted_at"
  end

  create_table "industries", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "locations", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "ratings", force: :cascade do |t|
    t.integer  "candidate_id"
    t.integer  "startup_id"
    t.float    "intrest_level"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.datetime "deleted_at"
  end

  create_table "startup_industries", force: :cascade do |t|
    t.integer "industry_id"
    t.integer "startup_id"
  end

  create_table "startup_locations", force: :cascade do |t|
    t.integer "location_id"
    t.integer "startup_id"
  end

  create_table "startups", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.string   "website_url"
    t.string   "image_url"
    t.string   "funding_round"
    t.integer  "number_of_employees"
    t.integer  "total_funding_raised"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.datetime "deleted_at"
    t.string   "crunchbase_url"
    t.string   "angellist_url"
  end

  create_table "view_logs", force: :cascade do |t|
    t.integer "email_id"
    t.integer "created_at"
  end

end
