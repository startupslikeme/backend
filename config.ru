# rubocop:disable Style/GlobalVars
require 'sinatra'
require 'rack/cors'
require 'grape'
require './routes/init'
require_relative 'app'

use Rack::Cors do
  allow do
    origins $website_url
    resource '*', headers: :any, methods: [:get, :post, :delete, :put, :patch, :options]
  end
end

use Rack::Session::Cookie
run Rack::Cascade.new [API, Web]
