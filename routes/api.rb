# rubocop:disable Metrics/BlockLength
require 'active_record'
require 'sinatra'
require 'rack/cors'
require 'grape'

# Resource Routes
class API < Grape::API
  version 'v1', using: :path
  format :json
  rescue_from ActiveRecord::RecordInvalid do |e|
    error!(e.record.errors, 400)
  end

  resource :candidates do
    desc 'Create a candidate.'
    params do
      requires :notification_method, type: String, desc: 'notification_method'
    end
    post do
      candidate = Candidate.new(
        frequency: params[:frequency] || 'weekly',
        notification_method: params[:notification_method]
      )
      candidate.onesignal_id = params[:onesignal_id] if params[:onesignal_id]
      candidate.onesignal_id = params[:email] if params[:email]
      candidate.save!
      candidate = Candidate.where(id: candidate.id)
      send_notifications_to(candidate)
      candidate.first
    end

    desc 'Update a candidate.'
    params do
      requires :id, type: String, desc: 'Candidate id.'
      requires :access_token, type: String, desc: 'Candidate access token.'
    end
    patch ':id' do
      candidate = Candidate.where(id: params[:id], access_token: params[:access_token]).first
      candidate.frequency = params[:frequency] if params[:frequency]
      candidate.save!
      candidate
    end
  end

  resource :startups do
    desc 'List of startups.'
    params do
      requires :access_token, type: String, desc: 'Candidate access token.'
      requires :email_id, type: Integer, desc: 'email id.'
    end
    get do
      candidate = Candidate.where(access_token: params[:access_token]).first
      email = Email.where(id: params[:email_id], candidate: candidate).first
      email.startups.as_json(include: %w(locations industries))
    end

    desc 'Create a startup.'
    params do
      requires :name, type: String, desc: 'Name of the startup'
      requires :website_url, type: String, desc: 'Website URL'
      # TODO: Have a proper way of authentication
      requires :api_key, type: String, desc: 'API Key to authenticate as internal service'
      requires :locations, type: Array[String]
      requires :industries, type: Array[String]
    end
    post do
      unless params[:api_key] == ENV['INTERNAL_API_KEY']
        error! 'API Key is wrong', 403
      end
      startup = Startup.create!(
        name: params[:name],
        website_url: params[:website_url],
        image_url: params[:image_url],
        number_of_employees: params[:number_of_employees],
        crunchbase_url: params[:crunchbase_url],
        angellist_url: params[:angellist_url],
        description: params[:description],
        total_funding_raised: params[:total_funding_raised]
      )

      params[:locations].each do |name|
        location = Location.where(name: name).first_or_create
        startup.locations << location
      end
      params[:industries].each do |name|
        industry = Industry.where(name: name).first_or_create
        startup.industries << industry
      end
    end
  end

  resource :ratings do
    desc 'List of ratings'
    params do
      requires :access_token, type: String, desc: 'Candidate access token.'
      requires :email_id, type: Integer, desc: 'email id.'
    end
    get do
      candidate = Candidate.where(access_token: params[:access_token]).first
      email = Email.where(id: params[:email_id], candidate: candidate).first
      Rating.where(
        candidate: candidate,
        startup: email.startups
      )
    end
    desc 'Create a rating.'
    params do
      requires :access_token, type: String, desc: 'Candidate access token'
      requires :startup_id, type: Integer, desc: 'Startup id'
      requires :intrest_level, type: Float, desc: 'Intrest level (0-1)'
    end
    post do
      candidate = Candidate.where(access_token: params[:access_token]).first
      startup = Startup.find(params[:startup_id])

      Rating.create!(
        candidate: candidate,
        startup: startup,
        intrest_level: params[:intrest_level]
      )
    end

    desc 'Update a rating'
    params do
      requires :id, type: Integer, desc: 'rating id'
      requires :access_token, type: String, desc: 'Candidate access token '
    end
    patch ':id' do
      rating = Rating.where(
        id: params[:id],
        candidate: Candidate.where(access_token: params[:access_token])
      ).first
      rating.intrest_level = params[:intrest_level] if params[:intrest_level]
      rating.save!
      rating
    end
  end
end
