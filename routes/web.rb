# rubocop:disable Style/GlobalVars
$website_url = if ENV['RACK_ENV'] == 'development'
                 'localhost:3000'
               else
                 'startupslike.me'
               end

# application routes
class Web < Sinatra::Base
  get '/' do
    redirect 'http://'.concat($website_url)
  end

  get '/ping' do
    'pong'
  end

  options '*' do
    response.headers['Allow'] = 'HEAD,GET,PUT,POST,DELETE,OPTIONS'
    response.headers['Access-Control-Allow-Headers'] = 'X-Requested-With, X-HTTP-Method-Override,'\
                                                       ' Content-Type, Cache-Control, Accept'
    200
  end
end
