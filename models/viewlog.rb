# to keep track of emails opened
class ViewLog < ActiveRecord::Base
  belongs_to :email
end
