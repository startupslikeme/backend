# model to store known industries
class Industry < ActiveRecord::Base
  has_many :startup_industry
  has_many :startups, through: :startup_industry
end
