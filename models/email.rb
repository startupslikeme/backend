# A record of every email sent by the app
class Email < ActiveRecord::Base
  belongs_to :candidate
  has_many :email_startup
  has_many :startups, through: :email_startup
end
