# model to store know locations
class Location < ActiveRecord::Base
  has_many :startup_location
  has_many :startups, through: :startup_location
end
