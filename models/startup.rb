# Important metrics on startups that will determine which ones are similar
class Startup < ActiveRecord::Base
  has_many :startup_industry
  has_many :industries, through: :startup_industry
  has_many :startup_location
  has_many :locations, through: :startup_location

  validates :name, presence: true
  validates :website_url, url: true
  validates :image_url, url: true
  # validates :funding_round,  inclusion: { in: %w(seed A B C IPO) }, unless: nil
  # validates :total_funding_raised, numericality: { greater_than_or_equal_to: 0 }, unless: nil
  # validates :number_of_employees, numericality: { greater_than_or_equal_to: 0 }, unless: nil
end
