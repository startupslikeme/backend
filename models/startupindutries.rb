# many to many model to represent a startups industries
class StartupIndustry < ActiveRecord::Base
  belongs_to :startup
  belongs_to :industry
end
