# a subscriber to our mailing list
class Candidate < ActiveRecord::Base
  belongs_to :authentication
  validates_uniqueness_of :email, allow_nil: true
  validates :email, email: true
  validates :frequency, presence: true, inclusion: { in: %w(never daily weekly monthly) }
  validates :notification_method, presence: true, inclusion: { in: %w(email web_push) }
  before_create :generate_access_token

  private

  def generate_access_token
    access_token = nil
    loop do
      access_token = SecureRandom.uuid
      break !self.class.exists?(access_token: access_token)
    end
    self.access_token = access_token
  end
end
