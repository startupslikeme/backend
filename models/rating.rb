# information on what a candidate likes/dislikes about a starup
class Rating < ActiveRecord::Base
  belongs_to :candidate
  belongs_to :startup
  validates :intrest_level, numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 1 }
end
