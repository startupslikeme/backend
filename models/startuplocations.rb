# many to many model to represent a startups locations
class StartupLocation < ActiveRecord::Base
  belongs_to :startup
  belongs_to :location
end
