# many to many model to represent a startup being included in an email
class EmailStartup < ActiveRecord::Base
  belongs_to :email
  belongs_to :startup
end
